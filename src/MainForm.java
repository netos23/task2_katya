import core.Solution;
import io.FileIO;
import util.ArrayUtils;
import util.JTableUtils;
import util.SwingUtils;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

public class MainForm extends JFrame {

	private JButton loadBtn;
	private JButton randomBtn;
	private JButton executeBtn;
	private JButton saveBtn;
	private JTable inputTable;
	private JTable outputTable;
	private JPanel mainContent;

	private JFileChooser fileChooserOpen;  // выбор директории
	private JFileChooser fileChooserSave;

	public MainForm() throws HeadlessException {
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("Смена столбцов");

		this.setContentPane(mainContent);
		this.setSize(620, 1000);
		this.pack();

		JTableUtils.initJTableForArray(inputTable, 100, true,
				true, true, true);
		inputTable.setRowHeight(25);
		JTableUtils.writeArrayToJTable(inputTable, new int[][]{
				{0, 2, 3,},
				{1, 4, 7},
				{5, 6, 8}
		});


		JTableUtils.initJTableForArray(outputTable, 100, false,
				false, false, false);
		outputTable.setRowHeight(25);


		fileChooserOpen = new JFileChooser();
		fileChooserSave = new JFileChooser();
		fileChooserOpen.setCurrentDirectory(new File("."));
		fileChooserSave.setCurrentDirectory(new File("."));
		FileFilter filter = new FileNameExtensionFilter("Text files", "txt");
		fileChooserOpen.addChoosableFileFilter(filter);
		fileChooserSave.addChoosableFileFilter(filter);

		fileChooserSave.setAcceptAllFileFilterUsed(false);
		fileChooserSave.setDialogType(JFileChooser.SAVE_DIALOG);
		fileChooserSave.setApproveButtonText("Save");

		loadBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent actionEvent) {

				try {
					if (fileChooserOpen.showOpenDialog(mainContent) == JFileChooser.APPROVE_OPTION) {
						int[][] arr = FileIO.readArray2FromFile(fileChooserOpen.getSelectedFile().getPath());
						JTableUtils.writeArrayToJTable(inputTable, arr);
					}
				} catch (Exception e) {
					SwingUtils.showErrorMessageBox(e);
				}
			}
		});

		randomBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent actionEvent) {
				try {
					int[][] matrix = ArrayUtils.createRandomIntMatrix(
							inputTable.getRowCount(), inputTable.getColumnCount(), 100);
					JTableUtils.writeArrayToJTable(inputTable, matrix);
				} catch (Exception e) {
					SwingUtils.showErrorMessageBox(e);
				}
			}
		});

		executeBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent actionEvent) {
				try {
					int[][] matrix = JTableUtils.readIntMatrixFromJTable(inputTable);
					if (matrix != null) {
						int[][] solve = Solution.solve(matrix);
						JTableUtils.writeArrayToJTable(outputTable, solve);
					}
				} catch (Exception e) {
					SwingUtils.showErrorMessageBox(e);
				}
			}
		});

		saveBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent actionEvent) {
				try {
					int[][] matrix = JTableUtils.readIntMatrixFromJTable(inputTable);
					if (fileChooserSave.showSaveDialog(mainContent) == JFileChooser.APPROVE_OPTION) {
						String file = fileChooserSave.getSelectedFile().getPath();
						if (!file.toLowerCase().endsWith(".txt")) {
							file += ".txt";
						}
						if(matrix != null) {
							FileIO.writeIntoFile(file, Solution.solve(matrix));
						}
					}
				} catch (Exception e) {
					SwingUtils.showErrorMessageBox(e);
				}
			}
		});
	}


}
