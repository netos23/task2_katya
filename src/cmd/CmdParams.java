package cmd;

public class CmdParams {  // создаем класс, который хранит параметры

	public String inputFile;
	public String outputFile;

	public boolean help;
	public boolean window;

	public static CmdParams parseArgs(String[] args) { // спорный момент про вывод ошибки
		CmdParams params = new CmdParams(); // создает объект, чтобы изменять параметры
		if (args.length == 0) {
			params.window = true;
			return params;
		}
		if (args.length == 2 && args[0].endsWith(".txt") && args[1].endsWith(".txt")) {
			params.inputFile = args[0];
			params.outputFile = args[1];
			return params;
		}
		int ArgsLength = args.length; // запоминаем 1 раз, чтобы лишний раз не обращаться
		for (int i = 0; i < ArgsLength; i++) {   // тут много лишних проверок, на зато тут не важен порядок указания параметров
			if (args[i].equals("--window") || args[i].equals("-w")) {
				params.window = true;
				return params;
			}
			if (args[i].equals("-h") || args[i].equals("--help")){
				params.help = true;
				return params;
			}
			if (args[i].equals("-i") || args[i].equals("--input")) {
				if (i + 1 == ArgsLength || !args[i + 1].endsWith(".txt")) {
					System.err.println("Missing Input file");
					System.exit(-1);
				}
				params.inputFile = args[i + 1];
			}
			if (args[i].equals("-o") || args[i].equals("--output")) {
				if (i + 1 > ArgsLength || !args[i + 1].endsWith(".txt")) {
					System.err.println("Missing Output file");
					System.exit(-1);
				}
				params.outputFile = args[i + 1];
			}

		}
		if ((params.inputFile == null || params.outputFile == null)) {
			System.err.println("Missing Input or Output file");
			System.exit(-1);
		}

		return params; // возвращаем объект с параметрами
	}

}