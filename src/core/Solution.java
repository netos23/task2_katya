package core;

public class Solution {
	public static int[][] solve(int[][] src) {
		// проверка границ
		if (src.length <=0 || src[0].length <= 0) {
			throw new ArrayIndexOutOfBoundsException("Матрица должна быть не пустой");
		}

		// примим мин и макс за первые колонки
		int maxC = 0, minC = 0;
		int maxEl = src[0][0], minEl = src[0][0];

		// обьявим матрицу такихже размеров
		int[][] res = new int[src.length][src[0].length];

		// поиск максимума и минимума
		for (int c = 0; c < src[0].length; c++) {
			// для каждой колонки вычисляем мин и макс
			int curMin = getMin(src, c);
			int curMax = getMax(src, c);

			// обновляем мин
			if (curMin <= minEl) {
				minEl = curMin;
				minC = c;
			}
			// обновляем макс
			if (curMax > maxEl) {
				maxEl = curMax;
				maxC = c;
			}
		}
		// копируем матрицу
		copy(src, res);
		//в скопированной матрице производим изменения
		swap(res, minC, maxC);
		return res;
	}
	// копирует матрицу
	private static void copy(int[][] src, int[][] target) {
		for (int r = 0; r < src.length; r++) {
			for (int c = 0; c < src[0].length; c++) {
				// соответственное копирование элементов
				target[r][c] = src[r][c];
			}
		}
	}

	// обменивает колонки
	private static void swap(int[][] src, int minC, int maxC) {
		// для всех рядов меняет соответственно элементы в колонках
		for (int r = 0; r < src.length; r++) {
			int tmp = src[r][minC];
			src[r][minC] = src[r][maxC];
			src[r][maxC] = tmp;
		}
	}

	// так же как и максимум см ниже
	static int getMin(int[][] src, int column) {
		if (src.length > 0 && column > src[0].length) {
			throw new ArrayIndexOutOfBoundsException();
		}
		int min = src[0][column];
		for (int r = 0; r < src.length; r++) {
			if (src[r][column] <= min) {
				min = src[r][column];
			}
		}

		return min;
	}
	/*
	* Поиск максимального элемента в выбранной колонке
	* */
	static int getMax(int[][] src, int column) {
		// проверка размера входит ли колонка в матрицу
		if (src.length > 0 && column > src[0].length) {
			throw new ArrayIndexOutOfBoundsException();
		}
		// примем за максимум первый элемент в колонке
		int max = src[0][column];
		// итерация по рядам с фиксированной колонкой
		for (int r = 0; r < src.length; r++) {
			if (src[r][column] >= max) {
				max = src[r][column];
			}
		}

		return max;
	}
	// Сотри если не нужно
	// Одновременный поиск максимума и минимума
	/*static ExtraPoints getExtraPoints(int[][] src, int column){
		if (column > src[0].length) {
			throw new ArrayIndexOutOfBoundsException();
		}
		int min = src[0][column];
		int max = src[0][column];
		for (int r = 0; r < src.length; r++) {
			if (src[r][column] <= min) {
				min = src[r][column];
			}

			if (src[r][column] >= max) {
				max = src[r][column];
			}
		}

		return new ExtraPoints(min,max);
	}

	static class ExtraPoints {
		int min;
		int max;

		public ExtraPoints(int min, int max) {
			this.min = min;
			this.max = max;
		}
	}*/
}
