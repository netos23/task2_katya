import cmd.CmdParams;
import core.Solution;
import io.FileIO;
import util.SwingUtils;

import java.util.Locale;

public class Main {

	public static void winMain() throws Exception {

		Locale.setDefault(Locale.ROOT);
		// установка шрифта
		SwingUtils.setDefaultFont("Microsoft Sans Serif", 18);
		// запуск формы
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				new MainForm().setVisible(true);
			}
		});

	}

	public static void main(String[] Args) throws Exception {
		// разбор аргументов
		CmdParams params = CmdParams.parseArgs(Args);
		// конфигурация по умолчанию при их отсутствии
		if (Args.length == 0 || params.window) {
			winMain();
			return;
		}
		// если вызвана помощь
		if (params.help) {
			System.out.println("Usage:");
			System.out.println("Fast execute <input-file> <output-file>");
			System.out.println();
			System.out.println("--input <input-file>// add Input file");
			System.out.println("-i <input-file>");
			System.out.println();
			System.out.println("--output <output-file> // add Output file");
			System.out.println("-o <output-file>");
			System.out.println();
			System.out.println("--window // show window");
			System.out.println("-w");
			System.out.println();
			System.out.println("If there are not any input-file and output-file");
			System.out.println("It will returns error in Error Stream");
			System.out.println();
			return;
		}
		// при  запуске без файлов работа в оконном режиме
		if (params.inputFile == null && params.inputFile == null) {
			winMain();
		} else {
			int[][] inp = FileIO.readArray2FromFile(params.inputFile);
			if (inp == null) {
				System.err.println("Can't read array");
			}

			FileIO.writeIntoFile(params.outputFile, Solution.solve(inp));
		}


	}
}
