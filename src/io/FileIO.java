package io;

import util.ArrayUtils;

import java.io.FileWriter;
import java.io.IOException;

public class FileIO {
	// клас для записи чтения матриц
	public static int[][] readArray2FromFile(String path) {
		return ArrayUtils.readIntArray2FromFile(path);
	}

	public static void writeIntoFile(String path, int[][] val) throws IOException {
		ArrayUtils.writeArrayToFile(path,val);
	}
}
